import React from "react";
import CustomForm from "../CustomForm/CustomForm";

export const PaymentForm = props => {
  const { onSuccessCallBack, onErrorCallBack, labels } = props;
  const onSubmit = () => {
    fetch("https://www.json-generator.com/api/json/get/cfeEkxZBNK?indent=2")
      .then(res => {
        if (onSuccessCallBack) {
          onSuccessCallBack();
        }
      })
      .catch(err => {
        if (onErrorCallBack) {
          onErrorCallBack();
        }
      });
  };
  return <CustomForm onSubmitAction={onSubmit} labels={labels} />;
};
