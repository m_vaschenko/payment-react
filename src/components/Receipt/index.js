import React from "react";
import './styles.css'

export const Receipt = props => {
  const {
    data: { items, hst, total }
  } = props;
  return (
    <div className={"receiptContainer"}>
      <p>Item Information</p>
      <div className="table">
        <div className="header">
          <div className={"header-item"}>Item</div>
          <div className={"header-item description"}>Description</div>
          <div className={"header-item"}>Qty</div>
          <div className={"header-item"}>Amount</div>
          <div className={"header-item"}>Price</div>
        </div>
        <div className="rows">
          {items.map(item => {
            const { name, description, subTotal, qty, amount } = item;
            return (
              <div className={"row"} key={name}>
                <div className={"row-item"}>{name}</div>
                <div className={"row-item description"}>{description}</div>
                <div className={"row-item qty"}>{qty}</div>
                <div className={"row-item amount"}>{amount}</div>
                <div className={"row-item"}>{subTotal}</div>
              </div>
            );
          })}
          <div className="row">
            <div className={"row-item"} />
            <div className={"row-item description"} />
            <div className={"row-item spec-title"}>HST</div>
            <div className={"row-item spec-value"}>{hst}</div>
          </div>
          <div className="row">
            <div className={"row-item"} />
            <div className={"row-item description"} />
            <div className={"row-item spec-title"}>Total</div>
            <div className={"row-item spec-value"}>{total}</div>
          </div>
        </div>
      </div>
    </div>
  );
};
