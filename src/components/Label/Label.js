// src/components/Task.js
import React from "react";
import "./Label.css";
export default function Label(_ref) {
  var _ref$data = _ref.data,
      id = _ref$data.id,
      text = _ref$data.text,
      type = _ref$data.type,
      flag = _ref$data.flag,
      onClickHandler = _ref.onClickHandler;
  return React.createElement("label", {
    className: "odessa-label".concat(type && "--" + type),
    htmlFor: id,
    onClick: onClickHandler
  }, text, flag && React.createElement("span", {
    className: "odessa-label__flag"
  }, " ", flag, " "));
}