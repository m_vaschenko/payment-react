import React from "react";
import TextInput from "ds-storybook/dist/components/TextInput/TextInput";
import Dropdown from "ds-storybook/dist/components/Dropdown/Dropdown";
import Button from "ds-storybook/dist/components/Button/Button";
import { Formik } from "formik";
import "./styles.css";

const Data = {
  labelType: "",
  labelFlag: "",
  hintText: "",
  hintId: "",
  inputChar: "",
  text: "Submit",
  skin: "primary"
};

const monthOpts = [
  { id: "1 - Jan", value: "1 - Jan" },
  { id: "2 - Feb", value: "2 - Feb" },
  { id: "3- Mar", value: "3- Mar" },
  { id: "4- Apr", value: "4- Apr" },
  { id: "5 - May", value: "5 - May" },
  { id: "6 - Jun", value: "6 - Jun" },
  { id: "7 - Jul", value: "7 - Jul" },
  { id: "8 - Aug", value: "8 - Aug" },
  { id: "9 - Sep", value: "9 - Sep" },
  { id: "10 - Oct", value: "10 - Oct" },
  { id: "11 - Nov", value: "11 - Nov" },
  { id: "12 - Dec", value: "12 - Dec" }
];

const yearOpts = [
  { id: 0, value: "2020" },
  { id: 1, value: "2021" },
  { id: 3, value: "2022" },
  { id: 4, value: "2023" }
];

const countriesOpts = [
  { id: "Canada", value: "Canada" },
  { id: "United States of America", value: "United States of America" }
];

const PaymentForm = props => {
  const { onSubmitAction, labels = {} } = props;

  const {
    firstName = "First Name",
    lastName = "Last Name",
    email = "Email",
    phone = "Phone",
    creditCard = "Credit Card",
    month = "Expiration date",
    cvv = "CVV",
    country = "Country",
    zip = "ZIP Code / Postal Code"
  } = labels;

  const customValidation = values => {
    const errors = {};
    if (!values.email) {
      errors.email = "Required";
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)) {
      errors.email = "Invalid email address";
    }
    if (
      !values.firstName ||
      (values.firstName && values.firstName.length < 2)
    ) {
      errors.firstName = "First name must be more than 2 symbols";
    }
    return errors;
  };

  return (
    <Formik
      initialValues={{
        firstName: "",
        lastName: "",
        email: "",
        month: "1 - Jan",
        year: "2020",
        country: "Canada"
      }}
      validate={customValidation}
      onSubmit={(values, { setSubmitting }) => {
        if (onSubmitAction) {
          onSubmitAction();
        }
      }}
    >
      {({
        values,
        errors,
        touched,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting
      }) => (
        <form onSubmit={handleSubmit} className={"odessa-PaymentForm"}>
          <TextInput
            data={{
              ...Data,
              labelText: firstName,
              value: values.firstName,
              name: "firstName",
              error: touched.firstName && errors.firstName
            }}
            onChange={handleChange}
            onBlur={handleBlur}
          />
          <TextInput
            data={{
              ...Data,
              labelText: lastName,
              value: values.lastName,
              name: "lastName"
            }}
            onChange={handleChange}
            onBlur={handleBlur}
          />
          <TextInput
            data={{
              ...Data,
              labelText: email,
              value: values.email,
              name: "email",
              error: touched.email && errors.email
            }}
            onChange={handleChange}
            onBlur={handleBlur}
          />
          <TextInput
            data={{
              ...Data,
              labelText: phone,
              value: values.phone,
              name: "phone"
            }}
            onChange={handleChange}
            onBlur={handleBlur}
          />
          <TextInput
            data={{
              ...Data,
              labelText: creditCard,
              inputChar: "",
              value: values.creditCard,
              name: "creditCard"
            }}
            onChange={handleChange}
            onBlur={handleBlur}
          />
          <div className={"odessa-PaymentForm-cardInfo-dateContainer"}>
            <div className="odessa-PaymentForm-cardInfo-dateContainer-left">
              <Dropdown
                data={{
                  ...Data,
                  labelText: month,
                  inputChar: 5,
                  options: monthOpts,
                  value: values.month,
                  name: "month"
                }}
                onChange={handleChange}
                onBlur={handleBlur}
              />
              <div style={{ alignSelf: "flex-end" }}>
                <Dropdown
                  data={{
                    ...Data,
                    labelText: "",
                    inputChar: 10,
                    options: yearOpts,
                    value: values.year,
                    name: "year"
                  }}
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
              </div>
            </div>
            <div className="odessa-PaymentForm-cardInfo-dateContainer-right">
              <TextInput
                data={{
                  ...Data,
                  labelText: cvv,
                  inputChar: 3,
                  value: values.cvv,
                  name: "cvv"
                }}
                onChange={handleChange}
                onBlur={handleBlur}
              />
            </div>
          </div>
          <Dropdown
            data={{
              ...Data,
              labelText: country,
              options: countriesOpts,
              value: values.country,
              name: "country"
            }}
            onChange={handleChange}
            onBlur={handleBlur}
          />
          <TextInput
            data={{
              ...Data,
              labelText: zip,
              inputChar: "",
              value: values.zip,
              name: "zip"
            }}
            onChange={handleChange}
            onBlur={handleBlur}
          />
          <div className="odessa-PaymentForm-buttonContainer">
            <Button
              data={{ ...Data }}
              onClickHandler={handleSubmit}
              disabled={isSubmitting}
            />
          </div>
        </form>
      )}
    </Formik>
  );
};

export default PaymentForm;
