import React from "react";
import { Receipt } from "./components/Receipt";
import { PaymentForm } from "./components/PaymentForm";
import "./App.css";

const data = {
  items: [
    {
      name: "SKU1",
      description: "description",
      subTotal: "22",
      qty: "1",
      amount: "2"
    },
    {
      name: "SKU2",
      description: "description2",
      subTotal: "22",
      qty: "1",
      amount: "2"
    }
  ],
  hst: "1",
  total: "111"
};

const App = props => {
  const { onSuccessCallBack, onErrorCallBack, userData = data } = props;
  return (
    <div className="App">
      <PaymentForm
        onSuccessCallBack={onSuccessCallBack}
        onErrorCallBack={onErrorCallBack}
      />
      <Receipt data={userData} />
    </div>
  );
};

export default App;
