import React from "react";
import { Receipt } from "./components/Receipt";
import { PaymentForm } from "./components/PaymentForm";
import "./App.css";
var data = {
  items: [{
    name: "SKU1",
    description: "description",
    subTotal: "22",
    qty: "1",
    amount: "2"
  }, {
    name: "SKU2",
    description: "description2",
    subTotal: "22",
    qty: "1",
    amount: "2"
  }],
  hst: "1",
  total: "111"
};

var App = function App(props) {
  var onSuccessCallBack = props.onSuccessCallBack,
      onErrorCallBack = props.onErrorCallBack,
      _props$userData = props.userData,
      userData = _props$userData === void 0 ? data : _props$userData;
  return React.createElement("div", {
    className: "App"
  }, React.createElement(PaymentForm, {
    onSuccessCallBack: onSuccessCallBack,
    onErrorCallBack: onErrorCallBack
  }), React.createElement(Receipt, {
    data: userData
  }));
};

export default App;