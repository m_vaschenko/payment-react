import React from "react";
import "../../../dist/other/Receipt/styles.css";
export var Receipt = function Receipt(props) {
  var _props$data = props.data,
      items = _props$data.items,
      hst = _props$data.hst,
      total = _props$data.total;
  return React.createElement("div", {
    className: "receiptContainer"
  }, React.createElement("p", null, "Item Information"), React.createElement("table", null, React.createElement("tr", null, React.createElement("th", null, "Item"), React.createElement("th", null, "Description"), React.createElement("th", null, "Qty"), React.createElement("th", null, "Amount"), React.createElement("th", null, "Subtotal")), items.map(function (item) {
    var name = item.name,
        description = item.description,
        subTotal = item.subTotal,
        qty = item.qty,
        amount = item.amount;
    return React.createElement("tr", null, React.createElement("td", null, name), React.createElement("td", {
      className: "description"
    }, description), React.createElement("td", null, qty), React.createElement("td", {
      className: "amount"
    }, amount), React.createElement("td", null, subTotal));
  }), React.createElement("tr", null, React.createElement("td", null, " "), React.createElement("td", null, " "), React.createElement("td", null, " "), React.createElement("td", null, "HST"), React.createElement("td", null, hst)), React.createElement("tr", null, React.createElement("td", null, " "), React.createElement("td", null, " "), React.createElement("td", null, " "), React.createElement("td", null, "Total"), React.createElement("td", null, total))));
};