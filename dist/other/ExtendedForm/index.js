import React from "react";
import PaymentForm from "../../components/PaymentForm/PaymentForm";
export var ExtendedForm = function ExtendedForm(props) {
  var onSuccessCallBack = props.onSuccessCallBack,
      onErrorCallBack = props.onErrorCallBack;

  var onSubmit = function onSubmit() {
    fetch("https://www.json-generator.com/api/json/get/cfeEkxZBNK?indent=2").then(function (res) {
      if (onSuccessCallBack) {
        onSuccessCallBack();
      }
    }).catch(function (err) {
      if (onErrorCallBack) {
        onErrorCallBack();
      }
    });
  };

  return React.createElement(PaymentForm, {
    onSubmitAction: onSubmit
  });
};