import React from "react";
import CustomForm from "../CustomForm/CustomForm";
export var PaymentForm = function PaymentForm(props) {
  var onSuccessCallBack = props.onSuccessCallBack,
      onErrorCallBack = props.onErrorCallBack,
      labels = props.labels;

  var onSubmit = function onSubmit() {
    fetch("https://www.json-generator.com/api/json/get/cfeEkxZBNK?indent=2").then(function (res) {
      if (onSuccessCallBack) {
        onSuccessCallBack();
      }
    }).catch(function (err) {
      if (onErrorCallBack) {
        onErrorCallBack();
      }
    });
  };

  return React.createElement(CustomForm, {
    onSubmitAction: onSubmit,
    labels: labels
  });
};