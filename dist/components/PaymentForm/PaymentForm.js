import _objectSpread from "/home/max/WebstormProjects/Storybook_new/node_modules/@babel/runtime/helpers/esm/objectSpread2";
import React from "react";
import TextInput from "../TextInput/TextInput";
import Dropdown from "../Dropdown/Dropdown";
import Button from "../Button/Button";
import { Formik, Form, Field, ErrorMessage } from "formik";
import "./styles.css";
var Data = {
  labelType: "",
  labelFlag: "",
  hintText: "",
  hintId: "",
  inputChar: "",
  text: "Submit",
  skin: "primary"
};
var monthOpts = [{
  id: "1 - Jan",
  value: "1 - Jan"
}, {
  id: "2 - Feb",
  value: "2 - Feb"
}, {
  id: "3- Mar",
  value: "3- Mar"
}, {
  id: "4- Apr",
  value: "4- Apr"
}, {
  id: "5 - May",
  value: "5 - May"
}, {
  id: "6 - Jun",
  value: "6 - Jun"
}, {
  id: "7 - Jul",
  value: "7 - Jul"
}, {
  id: "8 - Aug",
  value: "8 - Aug"
}, {
  id: "9 - Sep",
  value: "9 - Sep"
}, {
  id: "10 - Oct",
  value: "10 - Oct"
}, {
  id: "11 - Nov",
  value: "11 - Nov"
}, {
  id: "12 - Dec",
  value: "12 - Dec"
}];
var yearOpts = [{
  id: 0,
  value: "2020"
}, {
  id: 1,
  value: "2021"
}, {
  id: 3,
  value: "2022"
}, {
  id: 4,
  value: "2023"
}];
var countriesOpts = [{
  id: "Canada",
  value: "Canada"
}, {
  id: "United States of America",
  value: "United States of America"
}];

var PaymentForm = function PaymentForm(props) {
  var onSubmitAction = props.onSubmitAction;
  return React.createElement(Formik, {
    initialValues: {
      firstName: "",
      lastName: "",
      email: "",
      month: "1 - Jan",
      year: "2020",
      country: "Canada"
    } // validate={values => {
    //   const errors = {};
    //   if (!values.email) {
    //     errors.email = "Required";
    //   } else if (
    //     !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
    //   ) {
    //     errors.email = "Invalid email address";
    //   }
    //   return errors;
    // }}
    ,
    onSubmit: function onSubmit(values, _ref) {
      var setSubmitting = _ref.setSubmitting;

      if (onSubmitAction) {
        onSubmitAction();
      }

      setTimeout(function () {
        alert(JSON.stringify(values, null, 2));
        setSubmitting(false);
      }, 400);
    }
  }, function (_ref2) {
    var values = _ref2.values,
        errors = _ref2.errors,
        touched = _ref2.touched,
        handleChange = _ref2.handleChange,
        handleBlur = _ref2.handleBlur,
        handleSubmit = _ref2.handleSubmit,
        isSubmitting = _ref2.isSubmitting;
    return React.createElement("form", {
      onSubmit: handleSubmit,
      className: "odessa-PaymentForm"
    }, React.createElement(TextInput, {
      data: _objectSpread({}, Data, {
        labelText: "First Name",
        value: values.firstName,
        name: "firstName"
      }),
      onChange: handleChange,
      onBlur: handleBlur
    }), React.createElement(TextInput, {
      data: _objectSpread({}, Data, {
        labelText: "Last Name",
        value: values.lastName,
        name: "lastName"
      }),
      onChange: handleChange,
      onBlur: handleBlur
    }), React.createElement(TextInput, {
      data: _objectSpread({}, Data, {
        labelText: "Email",
        value: values.email,
        name: "email"
      }),
      onChange: handleChange,
      onBlur: handleBlur
    }), React.createElement(TextInput, {
      data: _objectSpread({}, Data, {
        labelText: "Phone",
        value: values.phone,
        name: "phone"
      }),
      onChange: handleChange,
      onBlur: handleBlur
    }), React.createElement(TextInput, {
      data: _objectSpread({}, Data, {
        labelText: "Credit Card",
        inputChar: "",
        value: values.creditCard,
        name: "creditCard"
      }),
      onChange: handleChange,
      onBlur: handleBlur
    }), React.createElement("div", {
      className: "odessa-PaymentForm-cardInfo-dateContainer"
    }, React.createElement("div", {
      className: "odessa-PaymentForm-cardInfo-dateContainer-left"
    }, React.createElement(Dropdown, {
      data: _objectSpread({}, Data, {
        labelText: "Expiration Date",
        inputChar: 5,
        options: monthOpts,
        value: values.month,
        name: "month"
      }),
      onChange: handleChange,
      onBlur: handleBlur
    }), React.createElement("div", {
      style: {
        alignSelf: "flex-end"
      }
    }, React.createElement(Dropdown, {
      data: _objectSpread({}, Data, {
        labelText: "",
        inputChar: 10,
        options: yearOpts,
        value: values.year,
        name: "year"
      }),
      onChange: handleChange,
      onBlur: handleBlur
    }))), React.createElement("div", {
      className: "odessa-PaymentForm-cardInfo-dateContainer-right"
    }, React.createElement(TextInput, {
      data: _objectSpread({}, Data, {
        labelText: "CVV",
        inputChar: 3,
        value: values.cvv,
        name: "cvv"
      }),
      onChange: handleChange,
      onBlur: handleBlur
    }))), React.createElement(Dropdown, {
      data: _objectSpread({}, Data, {
        labelText: "Country",
        options: countriesOpts,
        value: values.country,
        name: "country"
      }),
      onChange: handleChange,
      onBlur: handleBlur
    }), React.createElement(TextInput, {
      data: _objectSpread({}, Data, {
        labelText: "ZIP Code / Postal Code",
        inputChar: "",
        value: values.zip,
        name: "zip"
      }),
      onChange: handleChange,
      onBlur: handleBlur
    }), React.createElement("div", {
      className: "odessa-PaymentForm-buttonContainer"
    }, React.createElement(Button, {
      data: _objectSpread({}, Data),
      onClickHandler: handleSubmit,
      disabled: isSubmitting
    })));
  });
};

export default PaymentForm;