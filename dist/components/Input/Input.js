import React from "react";
import "./Input.css";
export default function Input(_ref) {
  var _ref$data = _ref.data,
      id = _ref$data.id,
      _char = _ref$data["char"],
      hintId = _ref$data.hintId,
      value = _ref$data.value,
      name = _ref$data.name,
      onChange = _ref.onChange,
      onBlur = _ref.onBlur;
  return React.createElement("input", {
    className: "odessa-input".concat(_char && "--" + _char + "-char-width"),
    "aria-describedby": hintId && "odessa-hint-".concat(hintId),
    type: "text",
    id: id,
    onBlur: onBlur,
    onChange: onChange,
    value: value,
    name: name
  });
}