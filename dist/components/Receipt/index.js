import React from "react";
import './styles.css';
export var Receipt = function Receipt(props) {
  var _props$data = props.data,
      items = _props$data.items,
      hst = _props$data.hst,
      total = _props$data.total;
  return React.createElement("div", {
    className: "receiptContainer"
  }, React.createElement("p", null, "Item Information"), React.createElement("div", {
    className: "table"
  }, React.createElement("div", {
    className: "header"
  }, React.createElement("div", {
    className: "header-item"
  }, "Item"), React.createElement("div", {
    className: "header-item description"
  }, "Description"), React.createElement("div", {
    className: "header-item"
  }, "Qty"), React.createElement("div", {
    className: "header-item"
  }, "Amount"), React.createElement("div", {
    className: "header-item"
  }, "Price")), React.createElement("div", {
    className: "rows"
  }, items.map(function (item) {
    var name = item.name,
        description = item.description,
        subTotal = item.subTotal,
        qty = item.qty,
        amount = item.amount;
    return React.createElement("div", {
      className: "row",
      key: name
    }, React.createElement("div", {
      className: "row-item"
    }, name), React.createElement("div", {
      className: "row-item description"
    }, description), React.createElement("div", {
      className: "row-item qty"
    }, qty), React.createElement("div", {
      className: "row-item amount"
    }, amount), React.createElement("div", {
      className: "row-item"
    }, subTotal));
  }), React.createElement("div", {
    className: "row"
  }, React.createElement("div", {
    className: "row-item"
  }), React.createElement("div", {
    className: "row-item description"
  }), React.createElement("div", {
    className: "row-item spec-title"
  }, "HST"), React.createElement("div", {
    className: "row-item spec-value"
  }, hst)), React.createElement("div", {
    className: "row"
  }, React.createElement("div", {
    className: "row-item"
  }), React.createElement("div", {
    className: "row-item description"
  }), React.createElement("div", {
    className: "row-item spec-title"
  }, "Total"), React.createElement("div", {
    className: "row-item spec-value"
  }, total)))));
};