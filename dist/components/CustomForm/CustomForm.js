import _objectSpread from "/home/max/WebstormProjects/payment-react/node_modules/@babel/runtime/helpers/esm/objectSpread2";
import React from "react";
import TextInput from "ds-storybook/dist/components/TextInput/TextInput";
import Dropdown from "ds-storybook/dist/components/Dropdown/Dropdown";
import Button from "ds-storybook/dist/components/Button/Button";
import { Formik } from "formik";
import "./styles.css";
var Data = {
  labelType: "",
  labelFlag: "",
  hintText: "",
  hintId: "",
  inputChar: "",
  text: "Submit",
  skin: "primary"
};
var monthOpts = [{
  id: "1 - Jan",
  value: "1 - Jan"
}, {
  id: "2 - Feb",
  value: "2 - Feb"
}, {
  id: "3- Mar",
  value: "3- Mar"
}, {
  id: "4- Apr",
  value: "4- Apr"
}, {
  id: "5 - May",
  value: "5 - May"
}, {
  id: "6 - Jun",
  value: "6 - Jun"
}, {
  id: "7 - Jul",
  value: "7 - Jul"
}, {
  id: "8 - Aug",
  value: "8 - Aug"
}, {
  id: "9 - Sep",
  value: "9 - Sep"
}, {
  id: "10 - Oct",
  value: "10 - Oct"
}, {
  id: "11 - Nov",
  value: "11 - Nov"
}, {
  id: "12 - Dec",
  value: "12 - Dec"
}];
var yearOpts = [{
  id: 0,
  value: "2020"
}, {
  id: 1,
  value: "2021"
}, {
  id: 3,
  value: "2022"
}, {
  id: 4,
  value: "2023"
}];
var countriesOpts = [{
  id: "Canada",
  value: "Canada"
}, {
  id: "United States of America",
  value: "United States of America"
}];

var PaymentForm = function PaymentForm(props) {
  var onSubmitAction = props.onSubmitAction,
      _props$labels = props.labels,
      labels = _props$labels === void 0 ? {} : _props$labels;
  var _labels$firstName = labels.firstName,
      firstName = _labels$firstName === void 0 ? "First Name" : _labels$firstName,
      _labels$lastName = labels.lastName,
      lastName = _labels$lastName === void 0 ? "Last Name" : _labels$lastName,
      _labels$email = labels.email,
      email = _labels$email === void 0 ? "Email" : _labels$email,
      _labels$phone = labels.phone,
      phone = _labels$phone === void 0 ? "Phone" : _labels$phone,
      _labels$creditCard = labels.creditCard,
      creditCard = _labels$creditCard === void 0 ? "Credit Card" : _labels$creditCard,
      _labels$month = labels.month,
      month = _labels$month === void 0 ? "Expiration date" : _labels$month,
      _labels$cvv = labels.cvv,
      cvv = _labels$cvv === void 0 ? "CVV" : _labels$cvv,
      _labels$country = labels.country,
      country = _labels$country === void 0 ? "Country" : _labels$country,
      _labels$zip = labels.zip,
      zip = _labels$zip === void 0 ? "ZIP Code / Postal Code" : _labels$zip;

  var customValidation = function customValidation(values) {
    var errors = {};

    if (!values.email) {
      errors.email = "Required";
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)) {
      errors.email = "Invalid email address";
    }

    if (!values.firstName || values.firstName && values.firstName.length < 2) {
      errors.firstName = "First name must be more than 2 symbols";
    }

    return errors;
  };

  return React.createElement(Formik, {
    initialValues: {
      firstName: "",
      lastName: "",
      email: "",
      month: "1 - Jan",
      year: "2020",
      country: "Canada"
    },
    validate: customValidation,
    onSubmit: function onSubmit(values, _ref) {
      var setSubmitting = _ref.setSubmitting;

      if (onSubmitAction) {
        onSubmitAction();
      }
    }
  }, function (_ref2) {
    var values = _ref2.values,
        errors = _ref2.errors,
        touched = _ref2.touched,
        handleChange = _ref2.handleChange,
        handleBlur = _ref2.handleBlur,
        handleSubmit = _ref2.handleSubmit,
        isSubmitting = _ref2.isSubmitting;
    return React.createElement("form", {
      onSubmit: handleSubmit,
      className: "odessa-PaymentForm"
    }, React.createElement(TextInput, {
      data: _objectSpread({}, Data, {
        labelText: firstName,
        value: values.firstName,
        name: "firstName",
        error: touched.firstName && errors.firstName
      }),
      onChange: handleChange,
      onBlur: handleBlur
    }), React.createElement(TextInput, {
      data: _objectSpread({}, Data, {
        labelText: lastName,
        value: values.lastName,
        name: "lastName"
      }),
      onChange: handleChange,
      onBlur: handleBlur
    }), React.createElement(TextInput, {
      data: _objectSpread({}, Data, {
        labelText: email,
        value: values.email,
        name: "email",
        error: touched.email && errors.email
      }),
      onChange: handleChange,
      onBlur: handleBlur
    }), React.createElement(TextInput, {
      data: _objectSpread({}, Data, {
        labelText: phone,
        value: values.phone,
        name: "phone"
      }),
      onChange: handleChange,
      onBlur: handleBlur
    }), React.createElement(TextInput, {
      data: _objectSpread({}, Data, {
        labelText: creditCard,
        inputChar: "",
        value: values.creditCard,
        name: "creditCard"
      }),
      onChange: handleChange,
      onBlur: handleBlur
    }), React.createElement("div", {
      className: "odessa-PaymentForm-cardInfo-dateContainer"
    }, React.createElement("div", {
      className: "odessa-PaymentForm-cardInfo-dateContainer-left"
    }, React.createElement(Dropdown, {
      data: _objectSpread({}, Data, {
        labelText: month,
        inputChar: 5,
        options: monthOpts,
        value: values.month,
        name: "month"
      }),
      onChange: handleChange,
      onBlur: handleBlur
    }), React.createElement("div", {
      style: {
        alignSelf: "flex-end"
      }
    }, React.createElement(Dropdown, {
      data: _objectSpread({}, Data, {
        labelText: "",
        inputChar: 10,
        options: yearOpts,
        value: values.year,
        name: "year"
      }),
      onChange: handleChange,
      onBlur: handleBlur
    }))), React.createElement("div", {
      className: "odessa-PaymentForm-cardInfo-dateContainer-right"
    }, React.createElement(TextInput, {
      data: _objectSpread({}, Data, {
        labelText: cvv,
        inputChar: 3,
        value: values.cvv,
        name: "cvv"
      }),
      onChange: handleChange,
      onBlur: handleBlur
    }))), React.createElement(Dropdown, {
      data: _objectSpread({}, Data, {
        labelText: country,
        options: countriesOpts,
        value: values.country,
        name: "country"
      }),
      onChange: handleChange,
      onBlur: handleBlur
    }), React.createElement(TextInput, {
      data: _objectSpread({}, Data, {
        labelText: zip,
        inputChar: "",
        value: values.zip,
        name: "zip"
      }),
      onChange: handleChange,
      onBlur: handleBlur
    }), React.createElement("div", {
      className: "odessa-PaymentForm-buttonContainer"
    }, React.createElement(Button, {
      data: _objectSpread({}, Data),
      onClickHandler: handleSubmit,
      disabled: isSubmitting
    })));
  });
};

export default PaymentForm;